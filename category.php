<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="container">

			<h1><?php _e( 'Categories for ', 'html5blank' ); single_cat_title(); ?></h1>

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

			<?php //get_sidebar(); ?>


		</section>
		<!-- /section -->
	</main>


<?php get_footer(); ?>
