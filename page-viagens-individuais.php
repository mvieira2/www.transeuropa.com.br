<?php get_header(); ?>

<main role="main">

	<section>

		<div class="margin-30-0">
			<div class="container">

				<div class="row ">

					<div class="col-sm-12 text-center">

						<h1 class="pristina">
							<?php the_title(); ?>
						</h1>

						<?php if (have_posts()): while (have_posts()) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; endif; ?>
					</div>


				</div>
			</div>
		</div>


		<div class="container">

			<?php get_template_part('mapa'); ?>

		</div>

		<div id="formulario" class="formulario no-print">
			
			<div class="container">

				<div class="row o-box-cinza">
					<div class="col-sm-12">
						<p class="text-center">
							Por favor, preencha o formulário e um dos nossos consultores entrará em contato o mais rápido possível!p
						</p>
					</div>
					<div class="col-sm-12">
						<?php 
						$formProd = do_shortcode( '[mc4wp_form id="363"]' );
						$formDev = do_shortcode( '[mc4wp_form id="362"]' );
						echo strpos($formProd,"error") ? $formDev : $formProd;
						?>

						<script type="text/javascript">

							$(".campo-destino, .campo-data, .campo-numpax, .campo-cathotel").removeClass("col-sm-4").addClass("col-sm-2")
							
							$('input[name="ORIGEM"]').val('Viagens individuais');

							function clickMapa(destino) {
								var target = $( "#formulario" );

								if( target.length ) {
									event.preventDefault();
									$('html, body').animate({
										scrollTop: target.offset().top - 150
									}, 500);
								}


								$('input[name="DESTINO"]').val(destino.dataset.destino);
							}
						</script>
						<script type="text/javascript">
							$("#mc4wp-form-1").submit(function () {
								var template1 = '<h3 style="font-family: arial,sans-serif; margin: auto;width: 980px;">Fomulário de contato</h3>  ============================================================================= <p style="font-family: arial,sans-serif; margin: auto;width: 980px;"> <b>Nome completo: </b> <br> {nome} <br><br> <b>E-mail: </b> <br> {email} <br><br> <b>Telefone: </b> <br> {telefone} <br><br> <b>Destino ou pacote desejado: </b> <br> {destino} <br><br> <b>Data da saída: </b> <br> {data} <br><br> <b>nº pessoas </b> <br> {num_pessoas} <br><br> <b>cat. hotel </b> <br> {categora} <br><br> <b>Como nos conheceu? </b> <br> {conheceu} <br><br> <b>Tipo de cliente: </b> <br> {tipo_cliente} <br><br> <b>Mensagem: </b> <br> {mensagem} </p>  ============================================================================='

								template1 = template1.replace("{nome}", $('input[name="NAME"').val() )
								template1 = template1.replace("{email}", $('input[name="EMAIL"').val() )
								template1 = template1.replace("{telefone}", $('input[name="TELEFONE"').val() )
								template1 = template1.replace("{destino}", $('input[name="DESTINO"').val() )
								template1 = template1.replace("{data}", $('input[name="MESVIAGEM"').val() )
								template1 = template1.replace("{num_pessoas}", $('input[name="NUMPAX"').val() )
								template1 = template1.replace("{categora}", $('input[name="CATHOTEL"').val() )
								template1 = template1.replace("{conheceu}", $('select[name="CONHECEU"').val() )
								template1 = template1.replace("{tipo_cliente}", $('input[name="TPCLIENTE"').val() )
								template1 = template1.replace("{mensagem}", $('textarea[name="MENSAGEM"').val() )

								enviarEmail(template1);
							})
						</script>
					</div>
				</div>
				

			</div>

		</div>

	</section>

</main>


<?php get_footer(); ?>
