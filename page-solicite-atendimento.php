<?php get_header(); ?>

<main role="main">

	<section>

		<div class=" margin-30-0">
			<div class="container">

				<div class="row ">

					<div class="col-sm-12 text-center">

						<h1 class="pristina">
							<?php the_title(); ?>
						</h1>

						<div id="formulario" class="formulario">
							
							

							<?php if (have_posts()): while (have_posts()) : the_post(); ?>
								<?php the_content(); ?>
							<?php endwhile; endif; ?>

						</div>
					</div>

					<script type="text/javascript">
						$('input[name="ORIGEM"]').val('Solicite Atendimento');
						$('.campo-data, .campo-numpax, .campo-numdias, .campo-conheceu').removeClass("col-sm-4").addClass("col-sm-2")
					</script>
					<script type="text/javascript">
							$("#mc4wp-form-1").submit(function () {
								var template1 = '<h3 style="font-family: arial,sans-serif; margin: auto;width: 980px;">Fomulário de contato</h3>  ============================================================================= <p style="font-family: arial,sans-serif; margin: auto;width: 980px;"> <b>Nome completo: </b> <br> {nome} <br><br> <b>E-mail: </b> <br> {email} <br><br> <b>Telefone: </b> <br> {telefone} <br><br> <b>Destino ou pacote desejado: </b> <br> {destino} <br><br> <b>Data da saída: </b> <br> {data} <br><br> <b>nº noites: </b> <br> {num_noites} <br><br> <b>nº pessoas </b> <br> {num_pessoas} <br><br> <b>Como nos conheceu? </b> <br> {conheceu} <br><br> <b>Tipo de cliente: </b> <br> {tipo_cliente} <br><br> <b>Mensagem: </b> <br> {mensagem} </p>  ============================================================================='

								template1 = template1.replace("{nome}", $('input[name="NAME"').val() )
								template1 = template1.replace("{email}", $('input[name="EMAIL"').val() )
								template1 = template1.replace("{telefone}", $('input[name="TELEFONE"').val() )
								template1 = template1.replace("{destino}", $('input[name="DESTINO"').val() )
								template1 = template1.replace("{data}", $('input[name="MESVIAGEM"').val() )
								template1 = template1.replace("{num_noites}", $('input[name="NUMDIAS"').val() )
								template1 = template1.replace("{num_pessoas}", $('input[name="NUMPAX"').val() )
								template1 = template1.replace("{conheceu}", $('select[name="CONHECEU"').val() )
								template1 = template1.replace("{tipo_cliente}", $('input[name="TPCLIENTE"').val() )
								template1 = template1.replace("{mensagem}", $('textarea[name="MENSAGEM"').val() )

								enviarEmail(template1);
							})
						</script>
				</div>
			</div>
		</div>


	</section>

</main>


<?php get_footer(); ?>
