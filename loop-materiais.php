
<?php 


$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

query_posts( array(
	'post_type'=> 'materiais',
	'posts_per_page' => -1,
	'paged' => $paged,
) ); 

?>
<div class="row loop-folhetos">
	
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<div id="post-<?php the_ID(); ?>" <?php post_class('col-sm-4'); ?> >
			<div class="box-estilo-2">



				<?php if(get_field('file')){?>
					<a target="_blank" href="<?php the_field('file'); ?>" title="<?php the_title(); ?>">
				<?php } else {?>
					<a  href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<?php }?>


					<figure>

						<?php if ( has_post_thumbnail()) :?>
							<?php the_post_thumbnail('full'); ?>
							<?php else: ?>
								<img src="https://placehold.it/360x180&text=No%20Image">

							<?php endif; ?>

						</figure>
						<p class="pristina text-center" >
							<?php the_title(); ?>
						</p>
					</a>
				</div>
			</div>


		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<div class="col-sm-12">
				<p class="alert alert-warning">
					<small>Nenhum item por aqui.</small>


				</p>
			</div>


			<!-- /article -->

		<?php endif; ?>

	</div>
