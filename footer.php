

<div class="inscrever-se form-plugin fundo-1">
	<div class="container">
		<div class="row">
			<label class="col-sm-4 control-label"><p class="texto-news">Receba nossas promoções e dicas de viagens:</p> </label>
			<div class="col-sm-8">
				
				<?php echo do_shortcode( '[yikes-mailchimp form="2"]' ); ?>
			</div>
		</div>
	</div>

</div>


<footer class="footer">
	<a class="btn botao-footer scrollTop">^</a>
	
	<?php

	$footer = get_page_by_path('footer'); 
	$content = apply_filters( 'the_content', $footer->post_content ); 
	echo $content;

	?>



	<?php wp_footer(); ?>


</body>
</html>
