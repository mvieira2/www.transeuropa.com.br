<?php get_header(); ?>

<main role="main">

	<section>


		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<?php the_content(); ?>


		<?php endwhile; endif; ?>

		<div class="margin-30-0">
			<div class="container">

				<div class="row ">

					<div class="col-sm-12 text-center">

						<h1 class="pristina">
							<?php the_title(); ?>
						</h1>
					</div>


				</div>
			</div>
		</div>


		<div class="container">

			<?php 


			query_posts( array(
				'post_type'=> 'produtos',
				'meta_key' => 'saida',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'category_name'  => "inclui-cruzeiro",
				'posts_per_page' => -1
			) ); 

			?>
			<?php get_template_part('loop-produtos'); ?>
			

		</div>



	</section>

</main>


<?php get_footer(); ?>
