var Utils = {

	Masks: function() {

		$('.data').unmask().mask('00/00/0000');
		$('.cep').unmask().mask('00000-000');
		$('.cnpj').unmask().mask('00.000.000/0000-00');
		$('.cpf').unmask().mask('000.000.000-00');
		$('.decimal').unmask().mask('#.##0,00', {reverse: true});
		$('.moedaComVirgula').unmask().mask('#.##0,00', {reverse: true});
		$('.moeda').unmask().mask('###0.00', {reverse: true});
		$('.pontos').unmask().mask('000.000.000.000.000.00');
		// $('.inteiro').text( $(this).text().replace('.00',''));


		var SPMaskBehavior = function (val) {
			return val.replace(/\D/g, '').length === 9 ? '(00) 00000-0000' : '(00) 0000-00009';
		},
		spOptions = {
			onKeyPress: function(val, e, field, options) {
				field.mask(SPMaskBehavior.apply({}, arguments), options);
			}
		};
		$('.tel').unmask().mask(SPMaskBehavior, spOptions);
	},



}

