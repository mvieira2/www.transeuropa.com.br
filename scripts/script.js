$(document).ready(function(){


	$('.header li.whatsapp').click(function () {
		$("#telHeader").toggle()
	});

	$('#labelSearch').click(function () {
		if ($("#pesquisar1").val() != '' ) {
			$("#searchHeader").submit();
			$("#pesquisar1").val('');
			return
		}
		$("#pesquisar1").toggleClass("opacity-1")
	});

	// Hide Header on on scroll down ---
	var didScroll;
	var lastScrollTop = 0;
	var delta = 5;
	var navbarHeight = $('.header > .linha-top').outerHeight();

	$(window).scroll(function(event){
		didScroll = true;

		if (didScroll) {
			hasScrolled();
			didScroll = false;
		}
	});


	function hasScrolled() {
		var st = $(this).scrollTop();

		if(Math.abs(lastScrollTop - st) <= delta)
			return;

			// If they scrolled down and are past the navbar, add class .nav-up.
			// This is necessary so you never see what is "behind" the navbar.
			if (st > lastScrollTop && st > navbarHeight){
			// Scroll Down
			$('.header').addClass('nav-up');
		} else {
			// Scroll Up
			if(st < lastScrollTop && st < navbarHeight) {
				$('.header').removeClass('nav-up');
			}
		}

		lastScrollTop = st;
	}


	$('.slider-1').slick({
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 3,
		prevArrow:'.slider-1-setas .seta-esquerda',
		nextArrow:'.slider-1-setas .seta-direita',
		responsive: [
		{
			breakpoint: 1024,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
				infinite: true,
			}
		},
		{
			breakpoint: 600,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2,
			}
		},
		{
			breakpoint: 480,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}]
	});


	$('.box-ofertas-hover-2 a[data-toggle="tab"]').click(function() {
		$('.box-ofertas-hover-2 a[data-toggle="tab"]').removeClass('active')
		$(this).addClass('active')
	})

	$(".zoom").fancybox({
		prevEffect	: 'none',
		nextEffect	: 'none',
		helpers	: {
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 50,
				height	: 50
			}
		}
	});

	$('.scrollTop').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
		return false;
	});

	$(".animate-link").on('click', function(event) {

		if (this.hash !== "") {

			event.preventDefault();

			var hash = this.hash;

			$('html, body').animate({
				scrollTop: Math.round($(hash).offset().top ) - 150
			}, 800, function () {
				$(this).animate({scrollTop: Math.round($(hash).offset().top ) - 150}, 800)
			});
		} 
	});
	$('.nav-tabs a, .bt_nav_menu').click(function (e) {
		e.preventDefault();
        $(this).tab('show');// bloqueado navegacao pelas barras do topo
    })

   Utils.Masks();

   function formatDate(date){
   	return date
   }

});
