<?php get_header(); ?>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/includes/moment/moment.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/includes/moment/moment-with-locales.js"></script>

<main role="main">

	<section>


		<div class="container">

			<div class="row ">

				<div class="col-sm-12 text-center">

					<h1 class="pristina">
						<?php the_title(); ?>
					</h1>

					<div class="textos">

						<?php if (have_posts()): while (have_posts()) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; endif; ?>

					</div>

					<?php /*

					<div class="row margin-30-0">
						<div class="col-sm-6 text-left" >
							<div id="tempo_riodejaneiro" class="row box-estilo-5">
								<div class="col-sm-7 padding-0">
									<div id="riodejaneiro" style="height:100%; height:250px;"></div>
								</div>
								<div class="col-sm-5">
									<h4 style="font-weight: 500;">Rio de Janeiro, Brasil</h4>
									<p class="data"></p>
									<img class="imagem" data-style="-webkit-filter: grayscale(100%); filter: grayscale(100%); " > 
									<p>
										<span class="graus" style="font-weight: 600;font-size: 27px;margin-top: 0px;"></span>
										<br>
										<span class="tempo"></span>
										<br>
										<span class="vento"></span>
										<br>
										<span class="umidade"></span>
										<br>
										<span class="visibilidade"></span>
									</p>
								</div>
							</div>

						</div>
						
						<div class="col-sm-6 text-left ">
							<h2 style="font-weight: 900;font-size: 27px;">
								Tel: <a href="tel:2122242297" style="font-weight: 900; font-size: 27px;">(21) 2224.2297</a>
							</h2>
							<h5 class="cor-3 semi-bold">
								RIO DE JANEIRO
							</h5>
							<br>
							<p>
								Rua Visconde de Pirajá, 550 - sala 229 - Ipanema - Rio de Janeiro - RJ <br>
								CEP: 22410-901
							</p>
							<br>
							<p>
								<b>Horário de atendimento:</b> 2ª a 6ª: 9h às 19h e sábados: 12h às 16h
							</p>
						</div>

					</div>
					<div class="row margin-30-0">

						<div class="col-sm-6 text-left" >
							<div id="tempo_saopaulo" class="row box-estilo-5">
								<div class="col-sm-7 padding-0">
									<div id="saopaulo" style="height:100%; height:250px;"></div>
								</div>
								<div class="col-sm-5">
									<h4 style="font-weight: 500;">São Paulo, Brasil</h4>
									<p class="data"></p>
									<img class="imagem" style="-webkit-filter: grayscale(100%); filter: grayscale(100%); " > 
									<p>
										<span class="graus" style="font-weight: 600;font-size: 27px;margin-top: 0px;"></span>
										<br>
										<span class="tempo"></span>
										<br>
										<span class="vento"></span>
										<br>
										<span class="umidade"></span>
										<br>
										<span class="visibilidade"></span>
									</p>
								</div>
							</div>

							
						</div>
						
						<div class="col-sm-6 text-left ">
							<h2 style="font-weight: 900;font-size: 27px;">
								Tel: <a href="tel:1131958727" style="font-weight: 900; font-size: 27px;">(11) 3195.8727</a>
							</h2>
							<h5 class="cor-3 semi-bold">
								SÃO PAULO
							</h5>
							<br>
							<p>
								Av. 09 de Julho, 5229 - 1º andar - Itaim - São Paulo - SP <br>
								CEP: 01407-907
							</p>
							<br>
							<p>
								<b>Horário de atendimento:</b> 2ª a 6ª: 9h às 19h e sábados: 12h às 16h
							</p>
						</div>
					</div>
					<div class="row margin-30-0">
						<div class="col-sm-6">
							

						</div>
						
						<div class="col-sm-6 text-left">
							<h2 style="font-weight: 900;font-size: 27px;margin-top: 0px;">
								Tel: <a href="tel:40031998" style="font-weight: 900; font-size: 27px;">4003.1998</a>
							</h2>
							<h5 class="cor-3 semi-bold">
								DEMAIS ESTADOS
							</h5>
							<p>
								
								<b>WhatsApp: (21) 98721-6977</b>
							</p>

						</div>


					</div>

					*/ ?>
				</div>

				<script type="text/javascript">
					$('input[name="ORIGEM"]').val('Solicite Atendimento');
					$('.campo-data, .campo-numpax, .campo-numdias, .campo-conheceu').removeClass("col-sm-4").addClass("col-sm-2")
				</script>

			</div>
		</div>
		<div class="box-estilo-3">


			<div id="formulario" class="formulario no-print">

				<div class="container">

					<div class="row o-box-cinza">
						<div class="col-sm-12">
							<p class="text-center">
								Para maiores informações sobre a Transeuropa ou algum de nossos produtos, entre em contato <br>
								por telefone ou pelo formulário abaixo. Teremos o maior prazer em atendê-lo!
							</p>
						</div>
						<div class="col-sm-12">
							<?php 
							$formProd = do_shortcode( '[mc4wp_form id="363"]' );
							$formDev = do_shortcode( '[mc4wp_form id="362"]' );
							echo strpos($formProd,"error") ? $formDev : $formProd;
							?>
							<script type="text/javascript">
								$('input[name="ORIGEM"]').val('Contato');

								$('.campo-name, .campo-email, .campo-telefone, .campo-conheceu').removeClass('col-sm-4').addClass('col-sm-6')			

							</script>

							<script type="text/javascript">
							$("#mc4wp-form-1").submit(function () {
								var template1 = '<h3 style="font-family: arial,sans-serif; margin: auto;width: 980px;">Fomulário de contato</h3>  =============================================================================  <p style="font-family: arial,sans-serif; margin: auto;width: 980px;"> <b>Nome completo: </b> <br> {nome} <br><br> <b>E-mail: </b> <br> {email} <br><br> <b>Telefone: </b> <br> {telefone} <br><br> <b>Como nos conheceu? </b> <br> {conheceu} <br><br> <b>Tipo de cliente: </b> <br> {tipo_cliente} <br><br> <b>Mensagem: </b> <br> {mensagem} </p>============================================================================='

								template1 = template1.replace("{nome}", $('input[name="NAME"').val() )
								template1 = template1.replace("{email}", $('input[name="EMAIL"').val() )
								template1 = template1.replace("{telefone}", $('input[name="TELEFONE"').val() )
								template1 = template1.replace("{conheceu}", $('select[name="CONHECEU"').val() )
								template1 = template1.replace("{tipo_cliente}", $('input[name="TPCLIENTE"').val() )
								template1 = template1.replace("{mensagem}", $('textarea[name="MENSAGEM"').val() )

								enviarEmail(template1);
							})
						</script>
						</div>
					</div>

				</div>
			</div>

		</div>

	</section>

</main>




<script>
	var map_riodejaneiro, map_saopaulo;
	var riodejaneiro = {lat: -22.9835795, lng: -43.211639};

	var saopaulo = {lat: -23.5790487, lng: -46.6757777};

	function initMap() {
		map_riodejaneiro = new google.maps.Map(document.getElementById('riodejaneiro'), {
			center: riodejaneiro,
			zoom: 15,
			disableDefaultUI: true,
		});

		var beachMarker = new google.maps.Marker({
			position: riodejaneiro,
			map: map_riodejaneiro,
			//icon: image,
			animation: google.maps.Animation.DROP,
		});

		map_saopaulo = new google.maps.Map(document.getElementById('saopaulo'), {
			center: saopaulo,
			zoom: 15,
			disableDefaultUI: true,
		});

		var beachMarker2 = new google.maps.Marker({
			position: saopaulo,
			map: map_saopaulo,
			//icon: image,
			animation: google.maps.Animation.DROP,
		});
	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB9CzY9RfUKkf9PxOV9J_Ldes1DQo2et_U&callback=initMap" ></script>

<script type="text/javascript">

	moment.locale('pt-BR');

	function localidade(id, lat,lon, callback) {

		$.get('https://api.openweathermap.org/data/2.5/weather?lang=pt&lat='+lat+'&lon='+lon+'&units=metric&appid=b231606340553d9174136f7f083904b3',{},function (res) {
			try {
				if(res){
					temp = res.main.temp;
					vento = 'Vento: ' + Math.round(res.wind.speed * 1.60934) + ' km/h';
					umidade = 'Umidade: ' + res.main.humidity + '%' ;
					visibilidade = 'Visibilidade: '+ Math.round(res.visibility * 1.60934) + ' km' ;
					image = '//openweathermap.org/img/w/'+res.weather[0].icon+'.png';
					tempo_text = tradutor(res.weather[0].description);


					data = moment().format("ddd, DD [de] MMM [de] YYYY");

            	temp = convertKelvinToCelsius(temp) + 'ºC'; //Kelvin to C


            	$(id).find('.data').html(data);
            	$(id).find('.imagem').attr('src', image);
            	$(id).find('.graus').html(temp);
            	$(id).find('.tempo').html(tempo_text);
            	$(id).find('.vento').html(vento);
            	$(id).find('.umidade').html(umidade);
            	$(id).find('.visibilidade').html(visibilidade);

            	if (typeof callback === 'function') {
            		callback()
            	}

            	

            }
        } catch(e) {
        	debugger
				$(id).find('.col-sm-7').addClass('col-sm-12').removeClass('col-sm-7');
				$(id).find('.col-sm-5').hide();


			}
		},'json');

	}

	
	localidade('#tempo_riodejaneiro', -22.9035, -43.2096, function () {
		localidade('#tempo_saopaulo', -23.5505,-46.6333)
	})
	function convertKelvinToCelsius(kelvin) {
		if (kelvin < (0)) {
			return 0;
		} else {
		//return (kelvin-273.15).toFixed(0);
		return (kelvin).toFixed(0);
	}
}

function tradutor(posicao) {
	return posicao;
}


</script>



<?php get_footer(); ?>
