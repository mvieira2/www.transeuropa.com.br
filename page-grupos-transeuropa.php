<?php get_header(); ?>

<main role="main">

	<section>

		<?php echo do_shortcode( '[smartslider3 slider=3]' );?>

		<div class="margin-30-0">
			<div class="container">

				<div class="row ">

					<div class="col-sm-12 text-center">

						<h1 class="pristina">
							<?php the_title(); ?>
						</h1>

						<div class="sub_title">
							<?php if (have_posts()): while (have_posts()) : the_post(); ?>

								<?php the_content(); ?>


							<?php endwhile; endif; ?>
						</div>
					</div>


				</div>
			</div>
		</div>


		<div class="container">

			
				<?php 

					get_template_part('loop-produtos-query');

				?>



		

		</div>



	</section>

</main>


<?php get_footer(); ?>
