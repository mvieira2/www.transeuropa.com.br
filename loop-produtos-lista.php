<div class="row">

	
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<div id="post-<?php the_ID(); ?>" <?php post_class('col-sm-12 linha-prods-lista'); ?> >


			<div class="row">
				<div class="col-sm-7 pristina nome-roteiro">
					<span class="seta-v"> > </span> <?php the_title(); ?>
				</div>
				<div class="col-sm-3 text-center duracao-roteiro">
					<i class="icon-info-pacote icon-duracao-negativo"></i> DURAÇÃO: <?php the_field('duracao'); ?> noites
				</div>
				<div class="col-sm-2 text-center botao-roteiro">
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="botao-5">SAIBA MAIS</a>
				</div>

			</div>


		</div>


	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<div class="col-sm-12">
			<p class="alert alert-warning"><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></p>

		</div>


		<!-- /article -->

	<?php endif; ?>

	<a href="<?php echo home_url('tours-regulares'); ?>">< Voltar para o mapa</a>

</div>
