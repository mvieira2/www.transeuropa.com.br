<div class="box-fale-conosco box-estilo-3">
	<div class="container">
		<div class="row text-center">
			<div class="col-sm-12">
				<h2 class="pristina text-center" style="font-size: 40px;">
					Fale conosco!
				</h2>
			</div>
			<div class="col-sm-4">
				<h2 style="font-weight: 900; font-size: 27px;">
					Tel: <a href="tel:2122242297" style="font-weight: 900; font-size: 27px;">(21) 2224.2297</a>
				</h2>
				<h5 class="cor-3 semi-bold">
					RIO DE JANEIRO
				</h5>
			</div>
			<div class="col-sm-4">
				<h2 style="font-weight: 900; font-size: 27px;">
					Tel: <a href="tel:1131958727" style="font-weight: 900; font-size: 27px;">(11) 3195.8727</a>
				</h2>
				<h5 class="cor-3 semi-bold">
					SÃO PAULO
				</h5>
			</div>
			<div class="col-sm-4">
				<h2 style="font-weight: 900; font-size: 27px;">
					Tel: <a href="tel:40031998" style="font-weight: 900; font-size: 27px;">4003.1998</a>
				</h2>
				<h5 class="cor-3 semi-bold">
					DEMAIS ESTADOS 
				</h5>
			</div>
			<div class="col-sm-12">
				<div class="clearfix"></div>
				<br>
				
				<h6>SOLICITE ATENDIMENTO</h6>
				<a class="botao-1" href="<?php echo home_url("solicite-atendimento");?>" style="padding: 6px 16px;margin-top: 2px;display: inline-block;border: none;font-weight: 400;">SOLICITAR</a>

				<br>
				<br>
			</div>
		</div>
			
	</div>

</div>