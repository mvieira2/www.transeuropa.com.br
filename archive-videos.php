<?php get_header();  

$other_page = get_page_by_path( 'videos' );


?>


<div class="banners" style="margin-bottom: 30px;">
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Wrapper for slides -->
		<div class="carousel-inner">

			<div class="item active">
				<img src="<?php echo get_template_directory_uri(); ?>/img/banner-videos.png">
			</div>

		</div>

	</div>
</div>


<main role="main">

	<section>

		<div class="margin-30-0">
			<div class="container">

				<div class="row ">

					<div class="col-sm-12 text-center">

						<h1 class="pristina">
							
							<?php echo get_the_title( $other_page ); ?>
							
						</h1>

						<?php echo $other_page->post_content; ?>

					</div>


				</div>
			</div>
		</div>


		<div class="container">

			<?php 


			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

			query_posts( array(
				'post_type'=> 'videos',
				//'category_name'  => 'documentacao',
				'posts_per_page' => -1,
				'paged' => $paged,
				//'page' => $paged
			) ); 

			?>

			<?php get_template_part('loop-videos'); ?>

			<!-- pagination -->
			<div class="pagination pull-right">
				<?php html5wp_pagination(); ?>
			</div>
			<!-- /pagination -->

		</div>


		<?php get_template_part('fale-conosco'); ?>


	</section>

</main>


<?php get_footer(); ?>
