<?php global $current_user; wp_get_current_user(); ?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>

	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
	<link href="//www.google-analytics.com" rel="dns-prefetch">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?php bloginfo('description'); ?>">

	<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/includes/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css?v=<?php echo get_version(); ?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/includes/slick/slick.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/includes/fonts/font-awesome-4.7.0/css/font-awesome.min.css">

	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/includes/jquery/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/includes/jquery/jquery.mask.min.js"></script>

	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/includes/bootstrap/js/bootstrap.min.js">	</script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/includes/slick/slick.min.js">	</script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/includes/fancybox/fancybox-2.1.7/jquery.fancybox.js">	</script>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/includes/fancybox/fancybox-2.1.7/jquery.fancybox.css">

	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/config.js"></script>

	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripts/script.js?v=<?php echo get_version(); ?>">	</script>

	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Felipa|Zilla+Slab:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">   

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

	<header class="header navbar-fixed-top">
		<div class="linha-top container desktop">
			<div class="row">
				<div class="col-sm-12">
					<ul class="nav navbar-nav redes-sociais navbar-right">

						<li class="area-do-agente">
							<?php if(!is_user_logged_in()){?>
								<a href="<?php echo home_url('area-do-agente'); ?>" class="text-uppercase">Área do agente de Viagem </a>
							<?php } else {?>
								<a href="<?php echo wp_logout_url( home_url() ); ?>" class="botao-sair">Olá, <?php echo $current_user->display_name; ?> <span style="font-weight: 300; margin-left: 10px;"> <i class="fa fa-arrow-circle-right	"></i> <span class="hover">sair</span> </span> </a>
							<?php } ?>
						</li>     
						<li class="whatsapp">

							<a target="_blank" id="telHeader" href="https://api.whatsapp.com/send?phone=5521987216977&text=Ol%C3%A1">(21) 98721.6977</a>
							<i class="fa fa-whatsapp" id="showHideTel" >
							</i>

						</li>                        
						<li class="instagram">
							<a target="_blank" href="https://www.instagram.com/transeuropaturismo/">
								<i class="fa fa-instagram">
								</i>
							</a>
						</li>        
						<li class="facebook">
							<a target="_blank" href="https://www.facebook.com/transeuropatur/">
								<i class="fa fa-facebook-f">
								</i>
							</a>
						</li>

						<li class="pesquisar">

							<form action="<?php echo home_url(); ?>" method="get" id="searchHeader">
								<label id="labelSearch" for="pesquisar1">
									<img src="<?php echo get_template_directory_uri(); ?>/img/icons/lupa.png" style="width: 17px; height: auto;">
								</label>
								<input type="text" id="pesquisar1" placeholder="pesquisar" name="s">
							</form>

						</li>

					</ul>

				</div>
			</div>

		</div>

		<nav class="navbar ">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle "  data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar">
						</span>
						<span class="icon-bar">
						</span>
						<span class="icon-bar">
						</span>
					</button>

					<a class="navbar-brand mobile" href="<?php echo home_url(); ?>">
						<img class="logo logo-mobile" src="<?php echo get_template_directory_uri(); ?>/img/logo.png?v1" alt="">
					</a>
				</div>
				<div class="collabitpse navbar-collapse collapse" id="myNavbar">
					<div class="row">
						<div class="col-sm-2">
							<a class="navbar-brand desktop" href="<?php echo home_url(); ?>">
								<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png?v1" class="logo " alt="">
							</a>
						</div>
						<div class="col-sm-12 mobile">
							<hr>
							<?php if(!is_user_logged_in()){?>
								<a href="<?php echo home_url('area-do-agente'); ?>">Área do agente de Viagem (logar)</a>
							<?php } else {?>
								<?php global $current_user; wp_get_current_user(); ?>
								<a href="<?php echo wp_logout_url( home_url() ); ?>" class="botao-sair">Olá, <?php echo $current_user->display_name; ?> <span style="font-weight: 300; margin-left: 10px;"> <i class="fa fa-arrow-circle-right	"></i> <span class="hover">sair</span> </span> </a>
							<?php } ?>
							<hr>
						</div>
						<div class="col-sm-10">

							<?php html5blank_nav(); ?>

							<div class="mobile">

								<?php if(is_user_logged_in()){?>
									<hr>
									<?php html5blank_nav_agente(); ?>
								<?php } ?>
								<hr>
								<div class="row" style="font-size: 20px;">
									<div class="col-xs-1">
										<a target="_blank" href="https://api.whatsapp.com/send?phone=5521987216977&text=Ol%C3%A1">
											<i class="fa fa-whatsapp" ></i>
										</a>
									</a>
								</div>
								<div class="col-xs-1 text-center">
									<a target="_blank" href="https://www.instagram.com/transeuropaturismo/">
										<i class="fa fa-instagram" > 
										</i>
									</a>

								</div>
								<div class="col-xs-1 text-center">
									<a target="_blank" href="https://www.facebook.com/transeuropatur/" >
										<i class="fa fa-facebook-f"></i>
									</a>
								</div>
								<div class="col-xs-9">
									<form action="<?php echo home_url(); ?>" method="get" >
										<div class="input-group">
											<div class="input-group-btn">
												<button class="btn btn-default" type="submit">
													<i class="glyphicon glyphicon-search"></i>
												</button>
											</div>
											<input type="text" class="form-control" placeholder="Pesquisar" name="s">
										</div>
									</form>
								</div>

							</div>
						</div>
					</div>

				</div>

			</div>

			<?php if(is_user_logged_in()){?>
				<div class="container desktop" style="position: relative;">

					<?php html5blank_nav_agente(); ?>
				</div>
			<?php }?>
		</div>
	</nav>

</header>

<?php if (!is_home() 
	&& !is_page("home") 
	&& !is_page("grupos-transeuropa") 
	&& !is_page("produtos-transeuropa")
	&& !is_page("cruzeiros")
	&& !is_archive("folhetos")
	&& !is_singular("folhetos")
	&& !is_singular("depoimentos")
	&& !is_singular("materiais")
	&& !is_singular("area-exclusiva")
	&& !is_singular("treinamentos")
	&& !is_singular("documentos")
	&& !is_singular("emails")
	&& !is_singular("videos")
) { ?>

	<?php


	if ( has_post_thumbnail() ) {
		?>

		<div class="banners">
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<!-- Wrapper for slides -->
				<div class="carousel-inner">

					<div class="item active">
						<?php the_post_thumbnail();?>
						<div class="carousel-caption hide">
							<div class="banner-titles">
								<h2 class="extra-bold" ><?php the_title();?></h2>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>

		<?php

	}
	else {
		echo '<div class="sem-banner teste-v2"></div>';
	}
	?>




<?php } ?>

<script type="text/javascript">

	function enviarEmail(message) {

		var data = { action:'siteWideMessage', message : message };

		$.post('<?php echo admin_url("admin-ajax.php"); ?>', data, function(response) {
			console.log(response);
		});
	}

</script>