<div class="row loop-depoimentos">
	
	<?php $count=0;  if (have_posts()): while (have_posts()) : the_post();  ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class('col-sm-12'); ?> >
		<div class="box-estilo-3">



			<!-- Left-aligned -->
			<div class="media">

				<div class="mobile media-left">
				<?php if ( has_post_thumbnail()) :?>
					<?php the_post_thumbnail('full', ['class' => 'media-object']); ?>
					<?php else: ?>
						<img src="https://placehold.it/360x180&text=No%20Image">

					<?php endif; ?>

				</div>

				<?php if ( $count % 2 == 0 ) :?>
					<div class="only-desktop media-left">
						<?php if ( has_post_thumbnail()) :?>
							<?php the_post_thumbnail('full', ['class' => 'media-object']); ?>
							<?php else: ?>
								<img src="https://placehold.it/360x180&text=No%20Image">

							<?php endif; ?>

						</div>
					<?php endif; ?>


					<div class="media-body">
						<h4 class="media-heading"><?php the_title(); ?></h4>
						<?php the_content(); ?>

						<?php if( get_field('link') ): ?>
							<a class="btn botao-1" href="<?php the_field('link'); ?>" title="<?php the_title(); ?>">
							SAIBA MAIS SOBRE ESSE ROTEIRO</a>
						<?php endif; ?>


					</div>

					<?php if ( $count % 2 != 0 ) :?>
						<div class="only-desktop media-right">
							<?php if ( has_post_thumbnail()) :?>
								<?php the_post_thumbnail('full', ['class' => 'media-object']); ?>
								<?php else: ?>
									<img src="https://placehold.it/360x180&text=No%20Image">

								<?php endif; ?>

							</div>
						<?php endif; ?>


					</div>

				</div>
			</div>


			<?php $count++; endwhile; ?>

			<?php else: ?>

				<!-- article -->
				<div class="col-sm-12">
					<p class="alert alert-warning">
						<small>Nenhum depoimento cadastrado.</small>


					</p>
				</div>


				<!-- /article -->

			<?php endif; ?>




		</div>
