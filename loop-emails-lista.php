<div class="row">

	<?php 

	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

	query_posts( array(
		'post_type'=> 'emails',
		'posts_per_page' => 10,
		'paged' => $paged,
	) ); 


	?>
	
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<div id="post-<?php the_ID(); ?>" <?php post_class('col-sm-12 linha-prods-lista'); ?> >


			<div class="row">
				<div class="col-sm-7 pristina nome-roteiro" style="font-weight: 500;">

					<?php if(get_field('url')){?>
						<a target="_blank" href="<?php the_field('url'); ?>" title="<?php the_title(); ?>" >
						<?php } else if(get_field('file')){?>
							<a target="_blank" href="<?php the_field('file'); ?>" title="<?php the_title(); ?>" >
							<?php } else {?>
								<a  href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" >
								<?php }?>

								<span class="seta-v"> > </span> <?php the_title(); ?>
							</a>
						</div>
						<div class="col-sm-3 text-center duracao-roteiro">
							<i class="icon-info-pacote icon-saidas-negativo-1"></i> ENVIADO: <?php the_field('data_de_envio'); ?>
						</div>
						<div class="col-sm-2 text-center botao-roteiro">

							<?php if(get_field('url')){?>
								<a target="_blank" href="<?php the_field('url'); ?>" title="<?php the_title(); ?>" class="botao-1 botao-1-sm">
								<?php } else if(get_field('file')){?>
									<a target="_blank" href="<?php the_field('file'); ?>" title="<?php the_title(); ?>" class="botao-1 botao-1-sm">
									<?php } else {?>
										<a  href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="botao-1 botao-1-sm">
										<?php }?>

									VISUALIZAR</a>

								</div>

							</div>


						</div>


					<?php endwhile; ?>

					<?php else: ?>

						<!-- article -->
						<div class="col-sm-12">
							<p class="alert alert-warning"><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></p>

						</div>


						<!-- /article -->

					<?php endif; ?>
					<div class="col-sm-12">
						<div class="pagination pull-right">
							<?php html5wp_pagination(); ?>
						</div>
					</div>

				</div>
