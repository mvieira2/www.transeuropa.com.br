<?php autenticar();  get_header();  


?>



<main role="main">

	<section>

		<div class="margin-30-0">
			<div class="container">

				<div class="row ">

					<div class="col-sm-12 text-center">

						<h1 class="pristina">
							<?php the_title(); ?>
						</h1>

						<?php if (have_posts()): while (have_posts()) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; endif; ?>
					</div>


				</div>
			</div>
		</div>




		<div class="container">

			<div class="row">


				<div class="col-sm-12">
					<div id="formulario" class="formulario no-print">

						<div class="container2">

							<div class="row box-cinza">

								<div class="col-sm-12">
									<?php 
									echo do_shortcode( '[contact-form-7 title="Formulário de contato Exclusivo"]' );
									?>
									
								</div>
							</div>

							<script type="text/javascript">
								$('[name="your-name"]').val("<?php echo $current_user->display_name; ?>")
								$('[name="your-email"]').val("<?php echo $current_user->user_email; ?>")
								$('[name="endereco"]').val("<?php echo get_user_meta($current_user->ID,'endereco',true);?>")
								$('[name="cidade"]').val("<?php echo get_user_meta($current_user->ID,'cidade',true);?>")
								$('[name="estado"]').val("<?php echo get_user_meta($current_user->ID,'estado',true);?>")
								$('[name="telefone"]').val("<?php echo get_user_meta($current_user->ID,'tel_ddd',true);?>")
								$('[name="cep"]').val("<?php echo get_user_meta($current_user->ID,'CEP',true);?>")

							</script>


						</div>

					</div>
				</div>
			</div>
			

		</div>



	</section>

</main>


<?php get_footer(); ?>
