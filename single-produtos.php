<?php get_header(); ?>

<main role="main">

	<section>

		<div class="bg-1 info-pacote">
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<p>
							<?php 
							$date = get_field('saida', false, false);
							$date = new DateTime($date);


							?>
							<i class="icon-info-pacote icon-saidas"></i>
							<span class="info-detail">SAÍDA: <?php echo get_field('periodo') ? the_field('periodo') : $date->format('d/m/Y'); ?> </span>
						</p>
					</div>
					<div class="col-sm-3 text-center">
						<p>
							<i class="icon-info-pacote icon-duracao"></i>
							<span class="info-detail">DURAÇÃO: <?php the_field('duracao'); ?> noites</span>
						</p>
					</div>
					<div class="col-sm-6 text-right">
						<p>
							<i class="icon-info-pacote icon-telefone"></i>
							<span class="info-detail">RJ: <a href="tel:(21) 2224.2297">(21) 2224.2297</a>  |  SP: <a href="tel:(11) 3195.8727">(11) 3195.8727</a>  |  DEMAIS ESTADOS <a href="tel:4003.1998">4003.1998</a></span>
						</p>
					</div>
				</div>


			</div>
			
		</div>

		<div class="container">
			
			<div class="row">

				<div class="col-sm-12">
					<div class="margin-top-30 row">
						<div class="col-sm-9 ">
							<h1 class="pristina">
								<?php the_title(); ?>
							</h1>
							<h4>
								<?php the_field('sub_titulo'); ?>
							</h4>
						</div>

						<div class="col-sm-3">
							<a href="#formulario" class="animate-link botao-1 btn margin-30-0"> SOLICITAR ESTA VIAGEM </a>

						</div>

					</div>
				</div>


				<div class="col-sm-5 col-xs-12 margin-bottom-20 pull-right no-print">
					<div class="row">
						<div class="col-sm-5 col-xs-6 pull-right hover">
							<?php if(get_field('upload_arquivo')) {?>
								<a href="<?php echo get_field('upload_arquivo'); ?>" target="_blank" class="pointer regular">
									<i class="glyphicon glyphicon-download-alt"></i>
									Imprimir dia a dia
								</a>	
							<?php } else { ?>
								<a onclick="window.print()" class="pointer regular">
									<i class="glyphicon glyphicon-print"></i>
									Imprimir
								</a>	
							<?php }?>

						</div>						
						<div class="col-sm-4 col-xs-3 pull-right text-right hover">
							<a data-toggle="modal" data-target="#myModal" class="pointer regular">
								<i class="glyphicon glyphicon-envelope"></i>
							Email</a> 
						</div>

					</div>


				</div>
			</div>


		</div>

		<div class="corpo-email">
			

			<div class="container pacote-detalhes">
				
				<ul class="nav nav-tabs nav-justified desktop">
					
					<?php if( get_field('saidas') ): ?>
						<li class="active hover">
							<style type="text/css">
								.pacote-detalhes ul.nav.nav-tabs>li a {
									font-size: 13px !important;
								}
							</style>
							<a data-toggle="tab" href="#saidas"> <i class="icon-info-pacote icon-saidas"></i> Saídas</a>
						</li>
					<?php endif; ?>							
					<?php if( get_field('dia_a_dia') ): ?>
						<li class="hover <?php echo get_field('dia_a_dia') && !get_field('saidas') ? 'active' : ''; ?>">
							<a data-toggle="tab" href="#menu1"> <i class="icon-info-pacote icon-roteiro"></i> Dia a dia</a>
						</li>
					<?php endif; ?>		

					<?php if( get_field('o_programa_inclui') ): ?>
						<li class="hover">
							<a data-toggle="tab" href="#menu2"><i class="icon-info-pacote icon-inclui"></i> O programa inclui</a>
						</li>
					<?php endif; ?>		

					<?php if( get_field('valores') ): ?>
						<li class="hover">
							<a data-toggle="tab" href="#menu3"><i class="icon-info-pacote icon-valores"></i> Valores</a>
						</li>
					<?php endif; ?>

					<?php if( get_field('hoteis') ): ?>
						<li class="hover">
							<a data-toggle="tab" href="#menu4"><i class="icon-info-pacote icon-hoteis"></i> Hotéis</a>
						</li>
					<?php endif; ?>

					<?php if( get_field('informacoes') ): ?>
						<li class="hover">
							<a data-toggle="tab" href="#menu5"><i class="icon-info-pacote icon-informacoes"></i> Informações</a>
						</li>
					<?php endif; ?>

					<?php if( get_field('documentos') ): ?>
						<li class="hover">
							<a data-toggle="tab" href="#menu6"><i class="icon-info-pacote icon-documentos"></i> Documentos</a>
						</li>
					<?php endif; ?>

					<?php if( get_field('condicoes_gerais') ): ?>
						<li class="hover">
							<a data-toggle="tab" href="#menu7"><i class="icon-info-pacote icon-condicoes-gerais"></i> Condições gerais</a>
						</li>
					<?php endif; ?>

				</ul>

				<div class="tab-content">

					<?php if( get_field('saidas') ): ?>
						<div class="mobile">
							<ul class="nav nav-stacked">
								<li class="active bg-1">
									<a  > <i class="icon-info-pacote icon-saidas"></i> Saídas</a>

								</li>

							</ul>
							<br>

						</div>

						<div id="saidas" class="collapse tab-pane fade in active">
							<?php the_field('saidas'); ?>
						</div>		
					<?php endif; ?>		
					
					<?php if( get_field('dia_a_dia') ): ?>
						<div class="mobile">
							<ul class="nav nav-stacked">
								<li class="active bg-1">
									<a  > <i class="icon-info-pacote icon-roteiro"></i> Dia a dia</a>

								</li>

							</ul>
							<br>

						</div>

						<div id="menu1" class="collapse tab-pane fade <?php echo get_field('dia_a_dia') && !get_field('saidas') ? 'in active	' : ''; ?>">
							<?php the_field('dia_a_dia'); ?>
						</div>

					<?php endif; ?>	
					<?php if( get_field('o_programa_inclui') ): ?>

						<div class="mobile">
							<ul class="nav nav-stacked ">
								<li class="active bg-1">
									<a ><i class="icon-info-pacote icon-inclui"></i> O programa inclui</a>

								</li>

							</ul>
							<br>
						</div>

						<div id="menu2" class="tab-pane fade">
							<?php the_field('o_programa_inclui'); ?>
						</div>
					<?php endif; ?>	

					<?php if( get_field('valores') ): ?>
						<div class="mobile">
							<ul class="nav nav-stacked ">
								<li class="active bg-1">
									<a><i class="icon-info-pacote icon-valores"></i> Valores</a>

								</li>

							</ul>
							<br>
						</div>

						<div id="menu3" class="tab-pane fade">
							<?php the_field('valores'); ?>
						</div>
					<?php endif; ?>


					<?php if( get_field('hoteis') ): ?>
						<div class="mobile">
							<ul class="nav nav-stacked ">
								<li class="active bg-1">
									<a><i class="icon-info-pacote icon-hoteis"></i> Hotéis</a>

								</li>

							</ul>
							<br>
						</div>

						<div id="menu4" class="tab-pane fade">
							<?php the_field('hoteis'); ?>
						</div>
					<?php endif; ?>

					<?php if( get_field('informacoes') ): ?>
						<div class="mobile">
							<ul class="nav nav-stacked ">
								<li class="active bg-1">
									<a ><i class="icon-info-pacote icon-informacoes"></i> Informações</a>

								</li>

							</ul>
							<br>
						</div>

						<div id="menu5" class="tab-pane fade">
							<?php the_field('informacoes'); ?>
						</div>
					<?php endif; ?>
					


					<?php if( get_field('documentos') ): ?>
						<div class="mobile">
							<ul class="nav nav-stacked ">
								<li class="active bg-1">
									<a><i class="icon-info-pacote icon-documentos"></i> Documentos</a>

								</li>

							</ul>
							<br>
						</div>

						<div id="menu6" class="tab-pane fade">
							<?php the_field('documentos'); ?>
						</div>
					<?php endif; ?>


					<?php if( get_field('condicoes_gerais') ): ?>

						<div class="mobile">
							<ul class="nav nav-stacked ">
								<li class="active bg-1">
									<a><i class="icon-info-pacote icon-condicoes-gerais"></i> Condições gerais</a>


								</li>

							</ul>
							<br>
						</div>

						<div id="menu7" class="tab-pane fade">
							<?php the_field('condicoes_gerais'); ?>
						</div>

					<?php endif; ?>





				</div>
			</div>


		</div>

		<div id="formulario" class="formulario no-print">
			
			<div class="container">

				<div class="row o-box-cinza">
					<div class="col-sm-12">
						<p class="text-center">
							Se você se interessou por esta viagem e quer saber mais detalhes, <br>
							preencha o formulário abaixo. Entraremos em contato em breve!
						</p>
					</div>
					<div class="col-sm-12 v2">
						<?php 
						//$formProd = do_shortcode( '[mc4wp_form id="363"]' );
						//$formDev = do_shortcode( '[mc4wp_form id="362"]' );
						//echo strpos($formProd,"error") ? $formDev : $formProd;
						//echo do_shortcode( '[mc4wp_form id="363"]' );
						
						echo do_shortcode( '[contact-form-7 title="Formulário de contato Destinos"]' );
						
						?>
						<script type="text/javascript">
							$('input[name="pacote-desejado"]').val('<?php the_title(); ?>');
						
						</script>
					</div>
				</div>
				

			</div>

		</div>


	</section>

</main>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header hide">
				<h3 class="modal-title">Receber no email</h3>
			</div>
			<div class="modal-body">
				<button type="button" class="close pull-right" data-dismiss="modal"><i class="fa fa-close"></i></button>
				<div class="row">
					<div class="col-sm-12 receber-no-email">
						<?php echo do_shortcode( '[contact-form-7 title="Send email"]' ); ?>
						<script>
							init()

							function init(){
								setTimeout(function(){

									if (!jQuery(".wpcf7").length) {
										init();
										return
									}

									jQuery('.receber-no-email[value="Como nos conheceu?"]').val('')

									jQuery('.receber-no-email[name="your-subject"], .receber-no-email [name="pacote-desejado"]').val('<?php the_title(); ?>')

									var textRoteiro = jQuery(`<?php echo sanitize_text_field(wp_strip_all_tags(
										the_field('dia_a_dia', false, false) .'\n\n O programa inclui: \n\n ' .
										the_field('o_programa_inclui', false, false). '\n\n Valores: \n\n ' .
										the_field('valores', false, false) . '\n\n Hotéis: \n\n  ' .
										the_field('hoteis', false, false) . '\n\n Informações: \n\n  ' . 
										the_field('informacoes', false, false) . '\n\n Documentos: \n\n  ' . 
										the_field('documentos', false, false) . '\n\n Condições Gerais: \n\n  ' . 
										the_field('condicoes_gerais', false, false)
										)); ?>`).text();

									jQuery('.receber-no-email [name="your-message"]').val(textRoteiro);						

								},750);

							}

							document.addEventListener( 'wpcf7submit', function( event ) {
							//var inputs = event.detail.inputs;



						}, false );

					</script>

				</div>
			</div>
		</div>
		<div class="modal-footer hide">

		</div>
	</div>

</div>

</div>


<?php get_footer(); ?>