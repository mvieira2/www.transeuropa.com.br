<?php get_header(); ?>

<main role="main">

	<section>

		<?php echo do_shortcode( '[smartslider3 slider=2]' );?>

		<div class="grupos margin-30-0">
			<div class="container">

				<div class="row home-links">
					<div class="col-sm-4">
						<div class="borderRightWhite">
							<?php echo get_field('campo_1');?>
						</div>

					</div>
					<div class="col-sm-4">
						<?php echo get_field('campo_2');?>
					</div>
					<div class="col-sm-4">
						<div class="borderLeftWhite">
						<?php echo get_field('campo_3');?>
							
						</div>
					</div>


				</div>
			</div>
		</div>


		<div class="container">
			<div class="margin-30-0 text-center">
				<h1 class="pristina cor-1">
					<?php the_title(); ?>
				</h1>

				<div class="cor-1 " style="font-size: 21px;text-transform: uppercase;font-weight: 200;margin-top: -20px;">

	
						<?php if (have_posts()): while (have_posts()) : the_post(); ?>

							<?php the_content(); ?>


						<?php endwhile; ?>

				</div> 
			</div>
		</div>


		<div class="container">
						
				<?php 

					get_template_part('loop-produtos-query');

				?>


		</div>

		<?php else: ?>

			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>


		<?php endif; ?>

	</section>

</main>


<?php get_footer(); ?>
