<div class="row">
<?php 
$args = array(
	'post_type'=> 'produtos',
	'meta_key' => 'saida',
	'orderby' => 'meta_value',
	'order' => 'ASC',
	'posts_per_page' => -1,
	'category__in' => array( get_cat_ID('Somente Terrestre') , get_cat_ID('Inclui Cruzeiro') ),
);


$query = new WP_Query( $args );


if ($query->have_posts()): while ($query->have_posts()) : $query->the_post(); ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class('col-sm-4'); ?> >
		<div class="box-estilo-1">
			<?php if( get_field('chamada') ): ?>

				<div class="filete-vermelho">
					<?php the_field('chamada'); ?>
				</div>


			<?php endif; ?>



			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">


				<?php if ( get_field('lista_de_imagens')) : ?>
					<figure style="background: url('<?php echo get_field('lista_de_imagens')[0]["url"]; ?>') left no-repeat; background-size: cover; "> 

						<?php else: ?>
							<figure style="background: url('https://placehold.it/360x360&text=No%20Image%20360x200') left no-repeat; background-size: cover; "> 

							<?php endif; ?>
							<div class="sombra"></div>

						</figure>
						<p class="pristina cor-3" >
							<?php the_title(); ?>
						</p>
					</a>
					<div class="clearfix"></div>
					<div class="row" style="margin: 0px 3px;background: #E6E6E7;">
						<div class="col-xs-2 text-center box-vermelho">
							<p>
								<?php the_field('duracao'); ?> noites
							</p>

						</div>
						<div class="col-xs-6 text-center">
							<?php 
							$date = get_field('saida', false, false);
							$date = new DateTime($date);


							?>
							<p style="padding-top: 18px;">
								<span style="font-weight: 400;">Saída:</span> <?php echo $date->format('d/m/Y'); ?>
							</p>
						</div>
						<div class="col-xs-4">
							<p style="padding-top: 15px; text-align: center; "> 
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="btn botao-2">
									SAIBA MAIS
								</a>
							</p>
						</div>

					</div>
				</div>
			</div>

		<?php endwhile; ?>

		<?php else: ?>
			<!-- article -->
			<div class="col-sm-12">
				<p class="alert alert-warning"><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></p>
			</div>
			<!-- /article -->
		<?php endif; ?>
</div>