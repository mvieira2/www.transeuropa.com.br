<?php 
/* Template Name: Page Campanha de Incentivo */ 
autenticar();  get_header();  
?>


<?php

global $wpdb;


$query = $wpdb->prepare("SELECT `meta_value` FROM `wp_postmeta` where `post_id` = ".get_the_ID()."  and `meta_key` LIKE ( SELECT REPLACE( (SELECT `meta_key` FROM  `wp_postmeta` where `post_id` = ".get_the_ID()." and `meta_key`  LIKE 'resultados_%_agente' and `meta_value` = '".$current_user->ID."' order by meta_id desc LIMIT 1) , 'agente', 'resultado') ) order by `meta_id` desc limit 1;", OBJECT );
$resultados = $wpdb->get_results( $query  )[0]->meta_value;

?>



<main role="main">

	<section>

		<div class="margin-30-0">
			<div class="container">

				<div class="row ">

					<div class="col-sm-12 text-center">

						<h1 class="pristina">
							<?php the_title(); ?>
						</h1>

						<?php if (have_posts()): while (have_posts()) : the_post(); ?>
							<?php the_field('sub_titulo'); ?>
						<?php endwhile; endif; ?>
					</div>


					<div class="col-sm-12">
						<br>

						<div class="corpo-email">


							<div class="container pacote-detalhes icones-mendiuns">

								<ul class="nav nav-tabs nav-justified-none desktop">

									<?php if( get_field('campanha') ): ?>
										<li class="active hover">
											<a data-toggle="tab" href="#campanha"> <i class="fa fa-bullhorn	"></i> Campanha</a>
										</li>
									<?php endif; ?>		
									<?php if( get_field('regulamento') ): ?>
										<li class=" ">
											<a data-toggle="tab" href="#regulamento"> <i class="fa fa-file-text-o"></i> Regulamento</a>
										</li>
									<?php endif; ?>				
									<?php if( get_field('premiacao') ): ?>
										<li class=" ">
											<a data-toggle="tab" href="#premiacao"> <i class="fa fa-gift"></i> Premiação</a>
										</li>
									<?php endif; ?>		


									<?php if( $resultados ): ?>
										<li class=" ">
											<a data-toggle="tab" href="#resultados"> <i class="fa fa-shirtsinbulk"></i> Ranking</a>
										</li>
									<?php endif; ?>							

								</ul>

								<div class="tab-content">

									<?php if( get_field('campanha') ): ?>
										<div class="mobile">
											<ul class="nav nav-stacked">
												<li class="active bg-1">
													<a  > <i class="fa fa-bullhorn	"></i> Campanha</a>

												</li>

											</ul>
											<br>

										</div>

										<div id="campanha" class="collapse tab-pane fade in active">
											<?php the_field('campanha'); ?>
										</div>		
									<?php endif; ?>		


									<?php if( get_field('regulamento') ): ?>
										<div class="mobile">
											<ul class="nav nav-stacked">
												<li class="active bg-1">
													<a  > <i class="fa fa-file-text-o"></i> Regulamento</a>

												</li>

											</ul>
											<br>

										</div>

										<div id="regulamento" class="collapse tab-pane fade">
											<?php the_field('regulamento'); ?>
										</div>		
									<?php endif; ?>		


									<?php if( get_field('premiacao') ): ?>
										<div class="mobile">
											<ul class="nav nav-stacked">
												<li class="active bg-1">
													<a  > <i class="fa fa-gift"></i> Premiação</a>

												</li>

											</ul>
											<br>

										</div>

										<div id="premiacao" class="collapse tab-pane fade">
											<?php the_field('premiacao'); ?>
										</div>		
									<?php endif; ?>		


									<?php if( $resultados ): ?>
										<div class="mobile">
											<ul class="nav nav-stacked">
												<li class="active bg-1">
													<a  > <i class="fa fa-shirtsinbulk"></i> Resultados</a>

												</li>

											</ul>
											<br>

										</div>

										<div id="resultados" class="collapse tab-pane fade">
											<?php

											echo $resultados;

											?>

										</div>		
									<?php endif; ?>		



								</div>
							</div>


						</div>
					</div>

				</div>
			</div>
		</div>

	</section>

</main>

<?php get_footer(); ?>
