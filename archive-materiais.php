<?php autenticar();  get_header();  

$page = get_page_by_path( 'materiais' );


?>


<div class="banners" style="margin-bottom: 30px;">
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Wrapper for slides -->
		<div class="carousel-inner">

			<div class="item active">

				<?php

				if ( has_post_thumbnail($page->ID) ) {
					echo get_the_post_thumbnail($page->ID, 'full');
				}
				else {
					echo '<img src="' . get_template_directory_uri()  . '/img/banner-folheto.png" />';
				}
				?>

			</div>

		</div>

	</div>
</div>


<main role="main">

	<section>

		<div class="margin-30-0">
			<div class="container2">

				<div class="row ">

					<div class="col-sm-12 text-center">

						<h1 class="pristina">
							
							<?php echo get_the_title( $page ); ?>
							
						</h1>

						<?php echo $page->post_content; ?>

					</div>


				</div>
			</div>
		</div>


		<div class="container">

			<div class="row">
				<div class="col-sm-12">
					<?php get_template_part('loop-materiais'); ?>
				</div>

				<div class="col-sm-12">
					<div id="formulario" class="formulario no-print">

						<div class="container2">

							<div class="row box-cinza">

								<div class="col-sm-12">
									<?php 
									echo do_shortcode( '[contact-form-7 title="Solicitar Material"]' );
									?>
									
								</div>
							</div>

							<script type="text/javascript">

								
								$('[name="nome_agencia"]').val("<?php echo get_user_meta($current_user->ID,'nome_da_agencia',true);?>")
								$('[name="your-name"]').val("<?php echo $current_user->display_name; ?>")
								$('[name="your-email"]').val("<?php echo $current_user->user_email; ?>")
								$('[name="endereco"]').val("<?php echo get_user_meta($current_user->ID,'endereco',true);?>")
								$('[name="cidade"]').val("<?php echo get_user_meta($current_user->ID,'cidade',true);?>")
								$('[name="estado"]').val("<?php echo get_user_meta($current_user->ID,'estado',true);?>")
								$('[name="telefone"]').val("<?php echo get_user_meta($current_user->ID,'tel_ddd',true);?>")
								$('[name="cep"]').val("<?php echo get_user_meta($current_user->ID,'CEP',true);?>")

							</script>


						</div>

					</div>
				</div>
			</div>
			

		</div>



	</section>

</main>


<?php get_footer(); ?>
