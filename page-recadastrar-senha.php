<?php autenticar(); get_header(); ?>


<main role="main">

	<section>


		<div class="container"  style="max-width: 900px;">

			<h1 class="pristina cor-1 text-center"><?php the_title(); ?></h1>
			<div class="cor-1">
				<?php if (have_posts()): while (have_posts()) : the_post(); ?>

					<?php the_content(); ?>


				<?php endwhile; endif; ?>


			</div>

			<div class="area-form">
				<?php
				global $wpdb;

				$error = '';
				$success = '';

				if( isset( $_POST['action'] ) && 'update' == $_POST['action'] && is_user_logged_in() && get_current_user_id() == $_POST['user_id'] ) 
				{
					


					if ($_POST['user_login_key'] === $_POST['user_login_key_confirm']) {

						$update_user = wp_set_password( $_POST['user_login_key'], get_current_user_id());
						$success = 'Senha atualizada com sucesso.';

					} else {
						$error = 'Oops Ocorreu um erro ao atualizar sua conta, a senha digitada está diferente da confirmada.';
					}


					if( ! empty( $error ) )
						echo '<div class="alert alert-danger"><p class="error"><strong>ERROR:</strong> '. $error .'</p></div>';

					if( ! empty( $success ) )
						echo '<div class="alert alert-success"><p class="success">'. $success .'</p></div>';
				}

				?>


				<form method="post" class="row margin-top-30" style="min-height: 125px;"> 
					<fieldset class="text-left">
						<p class="col-sm-4">
							<input type="hidden" name="user_id" value="<?php echo get_current_user_id();?>">
							<input type="password" placeholder="*Senha" class="form-control" name="user_login_key" id="user_login" value="" />
						</p>
						<p class="col-sm-4">
							<input type="password" placeholder="*Confirme a senha" class="form-control" name="user_login_key_confirm" id="user_login_key_confirm" value="" />
						</p>
						<p class="col-sm-4">
							<input type="hidden" name="action" value="update" />
							<input type="submit" value="Continuar" class="button botao-1-sm" id="submit" />
						</p>
						<p class="col-sm-12">*Campos obrigatórios.</p>
					</fieldset> 
				</form>
			</div>

			<p>
				Esta é uma área exclusiva para profissionais do turismo cadastrados com a Transeuropa, todo cadastro e acesso será verificado e passado por uma análise para aprovação do mesmo.
			</p>



		</section>

	</main>

	<?php get_footer(); ?>