<?php get_header(); ?>


<main role="main">

	<section>


		<div class="container" style="max-width: 900px;">
			
			<h1 class="pristina cor-1 text-center"><?php the_title(); ?></h1>
			<div class="cor-1 text-center">
				<?php if (have_posts()): while (have_posts()) : the_post(); ?>

					<?php the_content(); ?>


				<?php endwhile; endif; ?>

			</div>
			
			<div class="area-form">
				<p>
					Já sou cadastrado
				</p>
				
				<?php wp_login_form(array(
					'redirect' => home_url('area-exclusiva'),
					'label_username' => __( '' ),
					'label_password' => __( '' )
					
				)); ?>
			</div>

			<p>
				Se você ainda não tem cadastro, <a href="<?php echo home_url("cadastro"); ?>"> <b class="cor-3">clique aqui!</b> </a>

			</p>


			<p>
				Esta é uma área exclusiva para profissionais do turismo cadastrados com a Transeuropa, todo cadastro e acesso será verificado e passado por uma análise para aprovação do mesmo.
			</p>

			
		</div>
		

	</section>
</main>

<script type="text/javascript">

	$("#loginform").addClass("row form-login");
	$(".login-username").addClass("col-sm-4 padding-0");
	$(".login-password").addClass("col-sm-4 padding-0").after('<div class="col-sm-4 text-left padding-0"><p style="margin:6px 0px;"> <a href="<?php echo home_url('esqueci-minha-senha'); ?>">esqueci minha senha</a> </p> </div> ');
	$(".login-remember ").addClass("col-sm-2 text-left padding-0");
	$(".login-submit").addClass("col-sm-12 text-left padding-0" );

	
	$("#user_login").attr("placeholder","Login (e-mail)").addClass("form-control");
	$("#user_pass").attr("placeholder", "Senha").addClass("form-control");

	$("#wp-submit").addClass("botao-1-sm")
	

</script>


<?php get_footer(); ?>