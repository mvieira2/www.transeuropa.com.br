<?php autenticar();  get_header();  

$page = get_page_by_path( 'emails' );


?>


<div class="banners" style="margin-bottom: 30px;">
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Wrapper for slides -->
		<div class="carousel-inner">

			<div class="item active">

				<?php

				if ( has_post_thumbnail($page->ID) ) {
					echo get_the_post_thumbnail($page->ID, 'full');
				}
				else {
					echo '<img src="' . get_template_directory_uri()  . '/img/banner-folheto.png" />';
				}
				?>

			</div>

		</div>

	</div>
</div>


<main role="main">

	<section>

		<div class="margin-30-0">
			<div class="container2">

				<div class="row ">

					<div class="col-sm-12 text-center">

						<h1 class="pristina">
							
							<?php echo get_the_title( $page ); ?>
							
						</h1>

						<?php echo $page->post_content; ?>

					</div>


				</div>
			</div>
		</div>


		<div class="container">

			<div class="row">
				<div class="col-sm-12">
					<?php get_template_part('loop-emails-lista'); ?>
				</div>

			</div>
			

		</div>



	</section>

</main>


<?php get_footer(); ?>
