<?php get_header(); ?>

<main role="main">

	<section>

		<div class="margin-30-0">
			<div class="container">

				<div class="row ">

					<div class="col-sm-12 text-center">

						<h1 class="pristina">
							<?php the_title(); ?>
						</h1>

						<?php if (have_posts()): while (have_posts()) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; endif; ?>
					</div>


				</div>
			</div>
		</div>

	<?php if(!isset($_GET['category']) ) { ?>
		<div class="container">

			<?php get_template_part('mapa'); ?>

			<script type="text/javascript">

				function clickMapa(destino) {

					switch (destino.id) {
						case "am_norte":
							window.location.href = "<?php echo home_url('tours-regulares'); ?>?category=america-do-norte";
							break;
						case "caribe":
							window.location.href = "<?php echo home_url('tours-regulares'); ?>?category=caribe";
							break;
						case "amsul":
							window.location.href = "<?php echo home_url('tours-regulares'); ?>?category=america-do-sul";
							break;
						case "africa":
							window.location.href = "<?php echo home_url('tours-regulares'); ?>?category=africa";
							break;
						case "europa":
							window.location.href = "<?php echo home_url('tours-regulares'); ?>?category=europa";
							break;
						case "asia":
							window.location.href = "<?php echo home_url('tours-regulares'); ?>?category=asia";
							break;
						case "oceania":
							window.location.href = "<?php echo home_url('tours-regulares'); ?>?category=oceania";
							break;
						default:
							window.location.href = "<?php echo home_url('tours-regulares'); ?>";
							break;
					}

				}
			</script>




		</div>
	<?php } else { ?>

		<div class="container">
			<?php 
			query_posts( array(
				'post_type'=> 'produtos',
				'category_name'  => $_GET['category'],
				'posts_per_page' => -1
			) ); 

			?>
			<?php get_template_part('loop-produtos-lista'); ?>
			
		</div>

	<?php } ?>



	</section>

</main>


<?php get_footer(); ?>
