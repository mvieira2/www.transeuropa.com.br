<?php get_header();

// query_posts( array( 
// 	'post_type' => 'produtos'
// ) );

 ?>

	<main role="main">
		<!-- section -->
		<section class="container">

			<h1><?php echo sprintf( __( '%s Search Results for ', 'html5blank' ), $wp_query->found_posts ); echo ' "'. get_search_query() .'"' ; ?></h1>

			<?php get_template_part('loop-produtos'); ?>

			<?php get_template_part('pagination'); ?>
			<?php //get_sidebar(); ?>

		</section>
		<!-- /section -->
	</main>



<?php get_footer(); ?>
