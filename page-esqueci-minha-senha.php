<?php get_header(); ?>


<main role="main">

	<section>


		<div class="container"  style="max-width: 900px;">

			<h1 class="pristina cor-1 text-center"><?php the_title(); ?></h1>
			<div class="cor-1">
				<?php if (have_posts()): while (have_posts()) : the_post(); ?>

					<?php the_content(); ?>


				<?php endwhile; endif; ?>


			</div>

			<?php
			global $wpdb;

			$error = '';
			$success = '';

        // check if we're in reset form
			if( isset( $_POST['action'] ) && 'reset' == $_POST['action'] ) 
			{
				$email = trim($_POST['user_login']);
				$mailOk = false;

				if( empty( $email ) ) {
					$error = 'Digite um nome de usuário ou endereço de e-mail..';
				} else if( ! is_email( $email )) {
					$error = 'Nome de usuário ou endereço de e-mail inválido.';
				} else if( ! email_exists( $email ) ) {
					$error = 'Não há nenhum usuário registrado com esse endereço de e-mail.';
				} else {

					$mailOk = retrieve_password2();
				};

				if( $mailOk ){

					$success = 'Verifique seu endereço de e-mail para sua nova senha.';
				}

				else {
					$error = 'Oops Ocorreu um erro ao atualizar sua conta.';
				}



				if( ! empty( $error ) )
					echo '<div class="alert alert-danger"><p class="error"><strong>ERROR:</strong> '. $error .'</p></div>';

				if( ! empty( $success ) )
					echo '<div class="alert alert-success"><p class="success">'. $success .'</p></div>';
			}
			?>


			<div class="area-form" style="margin-top:70px;">
				<form method="post" class="row margin-top-30" style="min-height: 125px;"> 
					<fieldset class="text-left">
						<p class="col-sm-4">
							<?php $user_login = isset( $_POST['user_login'] ) ? $_POST['user_login'] : ''; ?>
							<input type="text" placeholder="E-mail" class="form-control" name="user_login" id="user_login" value="<?php echo $user_login; ?>" /></p>
							<p class="col-sm-6">
								<input type="hidden" name="action" value="reset" />
								<input type="submit" value="Continuar" class="button botao-1-sm" id="submit" />
							</p>
							<p class="col-sm-12">Você receberá no e-mail cadastrado um link com sua nova senha.</p>
						</fieldset> 
					</form>
				</div>

				<p>
					Esta é uma área exclusiva para profissionais do turismo cadastrados com a Transeuropa, todo cadastro e acesso será verificado e passado por uma análise para aprovação do mesmo.
				</p>



			</section>

		</main>

		<?php get_footer(); ?>