<?php get_header(); ?>

<div class="banners">
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active">
			</li>
			<li data-target="#myCarousel" data-slide-to="1">
			</li>
			<li data-target="#myCarousel" data-slide-to="2">
			</li>
		</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner">

			<div class="item active">
				<img src="<?php echo get_template_directory_uri(); ?>/img/banner-home.png" alt="Los Angeles">
				<div class="carousel-caption">
					<div class="banner-titles">
						<h2 class="extra-bold" >BEM-VINDO </h2>
					</div>
					<p class="banner-descritions">
						Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
					</p>
					<p class="text-center">
						<a href="#" class="btn botao-1">SOBRE NÓS</a>

					</p>
				</div>
			</div>

			<div class="item ">
				<img src="<?php echo get_template_directory_uri(); ?>/img/banner-home.png" alt="Los Angeles">
				<div class="carousel-caption">
					<div class="banner-titles">
						<h2 class="extra-bold" >BEM-VINDO AO  </h2>
					</div>
					<p class="banner-descritions">
						Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
					</p>
					<p class="text-center">
						<a href="#" class="btn botao-1">SOBRE NÓS</a>

					</p>
				</div>
			</div>

			<div class="item ">
				<img src="<?php echo get_template_directory_uri(); ?>/img/banner-home.png" alt="Los Angeles">
				<div class="carousel-caption">
					<div class="banner-titles">
						<h2 class="extra-bold" >BEM-VINDO  </h2>
					</div>
					<p class="banner-descritions">
						Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
					</p>
					<p class="text-center">
						<a href="#" class="btn botao-1">SOBRE NÓS</a>

					</p>
				</div>
			</div>

		</div>

	</div>
</div>

<div class="grupos margin-30-0">
	<div class="container">
		
		<div class="row ">
			<a href="#" class="col-sm-6 active">
				<div class="borderRightWhite">

					<p>
						GRUPOS COM GUIA BRASILEIRO
					</p>
					<p>
						Roteiros com hospedagem de primeira categoria,
						com guia brasileiro desde a saída do Brasil até o retorno.
					</p>

				</div>
			</a>
			<a href="#" class="col-sm-6">
				<div class=" borderLeftWhite">

					<p>
						CRUZEIROS MARÍTIMOS E FLUVIAIS
					</p>
					<p>
						Roteiros incluindo os melhores navios do mundo,
						com guia brasileiro desde a saída do Brasil até o retorno.
					</p>

				</div>
			</a>

		</div>
	</div>
</div>


<?php get_footer(); ?>
