<div class="row loop-depoimentos">
	
	<?php $count=0;  if (have_posts()): while (have_posts()) : the_post();  ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class('col-sm-6'); ?> >
		<div class="box-estilo-4">
			<?php the_content(); ?>
		</div>

		<script type="text/javascript">
			$('#post-<?php the_ID(); ?>').find("iframe, video").after('<h4 class="media-titulo"><?php the_title(); ?></h4>');
		</script>
	</div>

	<?php $count++; endwhile; ?>

	<?php else: ?>


		<div class="col-sm-12">
			<p class="alert alert-warning">
				<small>Nenhum vídeo cadastrado.</small>


			</p>
		</div>




	<?php endif; ?>




</div>

