<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
  External Modules/Files
  \*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
  Theme Support
  \*------------------------------------*/

  if (!isset($content_width))
  {
    $content_width = 900;
  }

  if (function_exists('add_theme_support'))
  {
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); 

    add_image_size('produtos', 550, 180, array( 'center', 'center' ) );
    add_image_size('produtos_boxes', 360, 360, array( 'center', 'center' ) );
    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
  'default-color' => 'FFF',
  'default-image' => get_template_directory_uri() . '/img/bg.jpg'
));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
  'default-image'     => get_template_directory_uri() . '/img/headers/default.jpg',
  'header-text'     => false,
  'default-text-color'    => '000',
  'width'       => 1000,
  'height'      => 198,
  'random-default'    => false,
  'wp-head-callback'    => $wphead_cb,
  'admin-head-callback'   => $adminhead_cb,
  'admin-preview-callback'  => $adminpreview_cb
));*/

    // Enables post and comment RSS feed links to head
add_theme_support('automatic-feed-links');

    // Localisation Support
load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
  Functions
  \*------------------------------------*/
  if ( ! file_exists( get_template_directory() . '/class-wp-bootstrap-navwalker.php' ) ) {
// file does not exist... return an error.
    return new WP_Error( 'class-wp-bootstrap-navwalker-missing', __( 'It appears the class-wp-bootstrap-navwalker.php file may be missing.', 'wp-bootstrap-navwalker' ) );
  } else {
// file exists... require it.
    require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
  }  

// HTML5 Blank navigation
  function html5blank_nav()
  {
   wp_nav_menu(
     array(
      'theme_location'  => 'header-menu',
      'depth'             => 2,
      'container'         => 'div',
      'container_class'   => 'collapse navbar-collapse',
      'container_id'      => 'bs-example-navbar-collapse-1',
      'menu_class'        => 'nav navbar-nav navbar-right',
      'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
      'walker'            => new WP_Bootstrap_Navwalker(),
    )
   );
 }
 function html5blank_nav_agente()
 {
   wp_nav_menu(
     array(
      'theme_location'  => 'header-menu-agente',
      'depth'             => 2,
      'container'         => 'div',
      'container_class'   => 'collapse navbar-collapse',
      'container_id'      => 'bs-example-navbar-collapse-1',
      'menu_class'        => 'nav navbar-nav menu-do-agente',
      'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
      'walker'            => new WP_Bootstrap_Navwalker(),
    )
   );
 }
 function html5blank_nav_agente_mobile()
 {
   wp_nav_menu(
     array(
      'theme_location'  => 'header-menu-agente',
      'depth'             => 2,
      'container'         => 'div',
      'container_class'   => 'collapse navbar-collapse',
      'container_id'      => 'bs-example-navbar-collapse-1',
      'menu_class'        => 'nav navbar-nav ',
      'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
      'walker'            => new WP_Bootstrap_Navwalker(),
    )
   );
 }

// Load HTML5 Blank scripts (header.php)
 function html5blank_header_scripts()
 {
  if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {


       // wp_register_script('html5blankscripts', get_template_directory_uri() . '/scripts/scripts.js', array('jquery'), '1.0.0'); // Custom scripts
        //wp_enqueue_script('html5blankscripts'); // Enqueue it!
  }
}

// Load HTML5 Blank conditional scripts
function html5blank_conditional_scripts()
{
  if (is_page('pagenamehere')) {
        wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
        wp_enqueue_script('scriptname'); // Enqueue it!
      }
    }

// Load HTML5 Blank styles
    function html5blank_styles()
    {

    //wp_register_style('html5blank', get_template_directory_uri() . '/css/style.css', array(), '1.0', 'all');
    //wp_enqueue_style('html5blank'); // Enqueue it!
    }

// Register HTML5 Blank Navigation
    function register_html5_menu()
    {
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
        'header-menu-agente' => __('Header Menu Agente', 'html5blank'), // Main Navigation
        'sidebar-menu' => __('Sidebar Menu', 'html5blank'), // Sidebar Navigation
        'extra-menu' => __('Extra Menu', 'html5blank') // Extra Navigation if needed (duplicate as many as you need!)
      ));
  }

// Remove the <div> surrounding the dynamic navigation to cleanup markup
  function my_wp_nav_menu_args($args = '')
  {
    $args['container'] = false;
    return $args;
  }

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
  function my_css_attributes_filter($var)
  {
    return is_array($var) ? array() : '';
  }

// Remove invalid rel attribute values in the categorylist
  function remove_category_rel_from_category_list($thelist)
  {
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
  }

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
  function add_slug_to_body_class($classes = array())
  {
    global $post;
    if (is_home()) {
      $key = array_search('blog', $classes);
      if ($key > -1) {
        unset($classes[$key]);
      }
    } elseif (is_page()) {
      $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
      $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
  }

// If Dynamic Sidebar Exists
  if (function_exists('register_sidebar'))
  {
    // Define Sidebar Widget Area 1
    register_sidebar(array(
      'name' => __('Widget Area 1', 'html5blank'),
      'description' => __('Description for this widget-area...', 'html5blank'),
      'id' => 'widget-area-1',
      'before_widget' => '<div id="%1$s" class="%2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h3>',
      'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
      'name' => __('Widget Area 2', 'html5blank'),
      'description' => __('Description for this widget-area...', 'html5blank'),
      'id' => 'widget-area-2',
      'before_widget' => '<div id="%1$s" class="%2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h3>',
      'after_title' => '</h3>'
    ));
  }

// Remove wp_head() injected Recent Comment styles
  function my_remove_recent_comments_style()
  {
    global $wp_widget_factory;
    remove_action('wp_head', array(
      $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
      'recent_comments_style'
    ));
  }

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin

  function html5wp_pagination()
  {
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
      'base' => str_replace($big, '%#%', get_pagenum_link($big)),
      'format' => '?paged=%#%',
      'current' => max(1, get_query_var('paged')),
      'total' => $wp_query->max_num_pages
    ));
  }

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
  return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
  return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
  global $post;
  if (function_exists($length_callback)) {
    add_filter('excerpt_length', $length_callback);
  }
  if (function_exists($more_callback)) {
    add_filter('excerpt_more', $more_callback);
  }
  $output = get_the_excerpt();
  $output = apply_filters('wptexturize', $output);
  $output = apply_filters('convert_chars', $output);
  $output = '<p>' . $output . '</p>';
  echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
  global $post;
  return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
  return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
  return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
  $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
  return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults)
{
  $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
  $avatar_defaults[$myavatar] = "Custom Gravatar";
  return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
  if (!is_admin()) {
    if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script('comment-reply');
    }
  }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
  $GLOBALS['comment'] = $comment;
  extract($args, EXTR_SKIP);

  if ( 'div' == $args['style'] ) {
    $tag = 'div';
    $add_below = 'comment';
  } else {
    $tag = 'li';
    $add_below = 'div-comment';
  }
  ?>
  <!-- heads up: starting < for the html tag (li or div) in the next line: -->
  <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
  <?php if ( 'div' != $args['style'] ) : ?>
   <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
   <?php endif; ?>
   <div class="comment-author vcard">
     <?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
     <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
   </div>
   <?php if ($comment->comment_approved == '0') : ?>
     <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
     <br />
   <?php endif; ?>

   <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
    <?php
    printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
    ?>
  </div>

  <?php comment_text() ?>

  <div class="reply">
   <?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
 </div>
 <?php if ( 'div' != $args['style'] ) : ?>
 </div>
<?php endif; ?>
<?php }

/*------------------------------------*\
  Actions + Filters + ShortCodes
  \*------------------------------------*/

// Add Actions
add_action('init', 'html5blank_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

/*------------------------------------*\
  Custom Post Types
  \*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
  function create_post_type_html5()
  {
    register_taxonomy_for_object_type('category', 'produtos'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'produtos');
    register_post_type('Produtos', // Register Custom Post Type
      array(
        'labels' => array(
            'name' => __('Produtos', 'html5blank'), // Rename these to suit
            'singular_name' => __('Produto', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New Produto', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit Produto', 'html5blank'),
            'new_item' => __('New Produto', 'html5blank'),
            'view' => __('View Produto', 'html5blank'),
            'view_item' => __('View Produto', 'html5blank'),
            'search_items' => __('Search Produto', 'html5blank'),
            'not_found' => __('No Produto', 'html5blank'),
            'not_found_in_trash' => __('No Produto found in Trash', 'html5blank')
          ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'menu_icon'   => 'dashicons-cart',
        'supports' => array(
          'title',
          'editor',
          'excerpt',
          'thumbnail'
        ), // Go to Dashboard Custom Produto post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
          'post_tag',
          'category'
        ) // Add Category and Post Tags support
      ));

        register_taxonomy_for_object_type('category', 'folhetos'); // Register Taxonomies for Category
        register_taxonomy_for_object_type('post_tag', 'folhetos');
        register_post_type('folhetos', // Register Custom Post Type
          array(
            'labels' => array(
            'name' => __('Folhetos', 'html5blank'), // Rename these to suit
            'singular_name' => __('Folheto', 'html5blank'),
            'add_new' => __('Adicionar Folheto', 'html5blank'),
            'add_new_item' => __('Adicionar Folheto', 'html5blank'),
            'edit' => __('Editar', 'html5blank'),
            'edit_item' => __('Editar Folheto', 'html5blank'),
            'new_item' => __('Novo Folheto', 'html5blank'),
            'view' => __('Ver companhia', 'html5blank'),
            'view_item' => __('Ver Folheto', 'html5blank'),
            'search_items' => __('Procurar Folheto', 'html5blank'),
            'not_found' => __('Folheto não encontrado', 'html5blank'),
            'not_found_in_trash' => __('Nenhuma Folheto encontrado no Lixo', 'html5blank')
          ),
            'public' => true,
            'menu_icon'   => 'dashicons-welcome-widgets-menus',
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
          'title',
          'editor',
            //'excerpt',
          'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
          'post_tag',
          'category'
        ) 
      ));

        register_taxonomy_for_object_type('category', 'area-exclusiva'); // Register Taxonomies for Category
        register_taxonomy_for_object_type('post_tag', 'area-exclusiva');
        register_post_type('area-exclusiva', // Register Custom Post Type
          array(
            'labels' => array(
            'name' => __('Área Exclusiva', 'html5blank'), // Rename these to suit
            'singular_name' => __('Área Exclusiva', 'html5blank'),
            'add_new' => __('Adicionar Item', 'html5blank'),
            'add_new_item' => __('Adicionar Item', 'html5blank'),
            'edit' => __('Editar', 'html5blank'),
            'edit_item' => __('Editar Item', 'html5blank'),
            'new_item' => __('Novo Item', 'html5blank'),
            'view' => __('Ver companhia', 'html5blank'),
            'view_item' => __('Ver Item', 'html5blank'),
            'search_items' => __('Procurar Item', 'html5blank'),
            'not_found' => __('Item não encontrado', 'html5blank'),
            'not_found_in_trash' => __('Nenhuma Item encontrado no Lixo', 'html5blank')
          ),
            'public' => true,
            'menu_icon'   => 'dashicons-businessman',
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
          'title',
          'editor',
            //'excerpt',
          'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
          'post_tag',
            //'category'
        ) 
      ));


        register_taxonomy_for_object_type('category', 'materiais'); // Register Taxonomies for Category
        register_taxonomy_for_object_type('post_tag', 'materiais');
        register_post_type('materiais', // Register Custom Post Type
          array(
            'labels' => array(
            'name' => __('Materiais', 'html5blank'), // Rename these to suit
            'singular_name' => __('Materiais', 'html5blank'),
            'add_new' => __('Adicionar Item', 'html5blank'),
            'add_new_item' => __('Adicionar Item', 'html5blank'),
            'edit' => __('Editar', 'html5blank'),
            'edit_item' => __('Editar Item', 'html5blank'),
            'new_item' => __('Novo Item', 'html5blank'),
            'view' => __('Ver companhia', 'html5blank'),
            'view_item' => __('Ver Item', 'html5blank'),
            'search_items' => __('Procurar Item', 'html5blank'),
            'not_found' => __('Item não encontrado', 'html5blank'),
            'not_found_in_trash' => __('Nenhuma Item encontrado no Lixo', 'html5blank')
          ),
            'public' => true,
            'menu_icon'   => 'dashicons-archive',
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
          'title',
          'editor',
            //'excerpt',
          'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
          'post_tag',
            //'category'
        ) 
      ));

  register_taxonomy_for_object_type('category', 'treinamentos'); // Register Taxonomies for Category
  register_taxonomy_for_object_type('post_tag', 'treinamentos');
        register_post_type('treinamentos', // Register Custom Post Type
          array(
            'labels' => array(
            'name' => __('Treinamentos', 'html5blank'), // Rename these to suit
            'singular_name' => __('Treinamentos', 'html5blank'),
            'add_new' => __('Adicionar Item', 'html5blank'),
            'add_new_item' => __('Adicionar Item', 'html5blank'),
            'edit' => __('Editar', 'html5blank'),
            'edit_item' => __('Editar Item', 'html5blank'),
            'new_item' => __('Novo Item', 'html5blank'),
            'view' => __('Ver companhia', 'html5blank'),
            'view_item' => __('Ver Item', 'html5blank'),
            'search_items' => __('Procurar Item', 'html5blank'),
            'not_found' => __('Item não encontrado', 'html5blank'),
            'not_found_in_trash' => __('Nenhuma Item encontrado no Lixo', 'html5blank')
          ),
            'public' => true,
            'menu_icon'   => 'dashicons-welcome-learn-more',
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
          'title',
          'editor',
            //'excerpt',
          'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
          'post_tag',
            //'category'
        ) 
      ));

          register_taxonomy_for_object_type('category', 'materiais'); // Register Taxonomies for Category
          register_taxonomy_for_object_type('post_tag', 'materiais');
        register_post_type('materiais', // Register Custom Post Type
          array(
            'labels' => array(
            'name' => __('Materiais', 'html5blank'), // Rename these to suit
            'singular_name' => __('Materiais', 'html5blank'),
            'add_new' => __('Adicionar Item', 'html5blank'),
            'add_new_item' => __('Adicionar Item', 'html5blank'),
            'edit' => __('Editar', 'html5blank'),
            'edit_item' => __('Editar Item', 'html5blank'),
            'new_item' => __('Novo Item', 'html5blank'),
            'view' => __('Ver companhia', 'html5blank'),
            'view_item' => __('Ver Item', 'html5blank'),
            'search_items' => __('Procurar Item', 'html5blank'),
            'not_found' => __('Item não encontrado', 'html5blank'),
            'not_found_in_trash' => __('Nenhuma Item encontrado no Lixo', 'html5blank')
          ),
            'public' => true,
            'menu_icon'   => 'dashicons-admin-media',
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
          'title',
          'editor',
            //'excerpt',
          'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
          'post_tag',
            //'category'
        ) 
      ));

          register_taxonomy_for_object_type('category', 'documentos'); // Register Taxonomies for Category
          register_taxonomy_for_object_type('post_tag', 'documentos');
        register_post_type('documentos', // Register Custom Post Type
          array(
            'labels' => array(
            'name' => __('Documentos', 'html5blank'), // Rename these to suit
            'singular_name' => __('Documentos', 'html5blank'),
            'add_new' => __('Adicionar Item', 'html5blank'),
            'add_new_item' => __('Adicionar Item', 'html5blank'),
            'edit' => __('Editar', 'html5blank'),
            'edit_item' => __('Editar Item', 'html5blank'),
            'new_item' => __('Novo Item', 'html5blank'),
            'view' => __('Ver companhia', 'html5blank'),
            'view_item' => __('Ver Item', 'html5blank'),
            'search_items' => __('Procurar Item', 'html5blank'),
            'not_found' => __('Item não encontrado', 'html5blank'),
            'not_found_in_trash' => __('Nenhuma Item encontrado no Lixo', 'html5blank')
          ),
            'public' => true,
            'menu_icon'   => 'dashicons-admin-media',
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
          'title',
          'editor',
            //'excerpt',
          'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
          'post_tag',
            //'category'
        ) 
      ));
        
          register_taxonomy_for_object_type('category', 'emails'); // Register Taxonomies for Category
          register_taxonomy_for_object_type('post_tag', 'emails');
        register_post_type('emails', // Register Custom Post Type
          array(
            'labels' => array(
            'name' => __('Emails Mkt', 'html5blank'), // Rename these to suit
            'singular_name' => __('Emails Mkt', 'html5blank'),
            'add_new' => __('Adicionar Item', 'html5blank'),
            'add_new_item' => __('Adicionar Item', 'html5blank'),
            'edit' => __('Editar', 'html5blank'),
            'edit_item' => __('Editar Item', 'html5blank'),
            'new_item' => __('Novo Item', 'html5blank'),
            'view' => __('Ver companhia', 'html5blank'),
            'view_item' => __('Ver Item', 'html5blank'),
            'search_items' => __('Procurar Item', 'html5blank'),
            'not_found' => __('Item não encontrado', 'html5blank'),
            'not_found_in_trash' => __('Nenhuma Item encontrado no Lixo', 'html5blank')
          ),
            'public' => true,
            'menu_icon'   => 'dashicons-email',
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
          'title',
          'editor',
            //'excerpt',
          'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
          'post_tag',
            //'category'
        ) 
      ));

        register_taxonomy_for_object_type('category', 'depoimentos'); // Register Taxonomies for Category
        register_taxonomy_for_object_type('post_tag', 'depoimentos');
        register_post_type('depoimentos', // Register Custom Post Type
          array(
            'labels' => array(
            'name' => __('Depoimentos', 'html5blank'), // Rename these to suit
            'singular_name' => __('Depoimento', 'html5blank'),
            'add_new' => __('Adicionar Depoimento', 'html5blank'),
            'add_new_item' => __('Adicionar Depoimento', 'html5blank'),
            'edit' => __('Editar', 'html5blank'),
            'edit_item' => __('Editar Depoimento', 'html5blank'),
            'new_item' => __('Novo Depoimento', 'html5blank'),
            'view' => __('Ver companhia', 'html5blank'),
            'view_item' => __('Ver Depoimento', 'html5blank'),
            'search_items' => __('Procurar Depoimento', 'html5blank'),
            'not_found' => __('Depoimento não encontrado', 'html5blank'),
            'not_found_in_trash' => __('Nenhuma Depoimento encontrado no Lixo', 'html5blank')
          ),
            'public' => true,
            'menu_icon'   => 'dashicons-clipboard',
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
          'title',
          'editor',
            //'excerpt',
          'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
          'post_tag',
          'category'
        ) 
      ));
        
        register_taxonomy_for_object_type('category', 'videos'); // Register Taxonomies for Category
        register_taxonomy_for_object_type('post_tag', 'videos');
        register_post_type('videos', // Register Custom Post Type
          array(
            'labels' => array(
            'name' => __('Vídeos', 'html5blank'), // Rename these to suit
            'singular_name' => __('Vídeo', 'html5blank'),
            'add_new' => __('Adicionar Vídeo', 'html5blank'),
            'add_new_item' => __('Adicionar Vídeo', 'html5blank'),
            'edit' => __('Editar', 'html5blank'),
            'edit_item' => __('Editar Vídeo', 'html5blank'),
            'new_item' => __('Novo Vídeo', 'html5blank'),
            'view' => __('Ver companhia', 'html5blank'),
            'view_item' => __('Ver Vídeo', 'html5blank'),
            'search_items' => __('Procurar Vídeo', 'html5blank'),
            'not_found' => __('Vídeo não encontrado', 'html5blank'),
            'not_found_in_trash' => __('Nenhuma Vídeo encontrado no Lixo', 'html5blank')
          ),
            'public' => true,
            'menu_icon'   => 'dashicons-format-video',
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
          'title',
          'editor',
            //'excerpt',
          'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
          'post_tag',
          'category'
        ) 
      ));

      }

/*------------------------------------*\
  ShortCode Functions
  \*------------------------------------*/

// Shortcode Demo with Nested Capability
  function html5_shortcode_demo($atts, $content = null)
  {
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
  }

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
  return '<h2>' . $content . '</h2>';
}

function my_added_page_content ( $content ) {
  if ( is_archive("videos") ) {
    return str_replace('<p>&nbsp;</p>','', $content );
  }

  return $content;
}
add_filter( 'the_content', 'my_added_page_content');

function sample_admin_notice__error() {

  if ( !is_plugin_active( 'mailchimp-for-wp/mailchimp-for-wp.php' )) {
    $class = 'notice notice-error';
    $message = 'O plugin "MailChimp for WP" é necessário estar instalado e ativo';

    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
  }
  if ( !is_plugin_active( 'bold-page-builder/bold-builder.php' )) {
    $class = 'notice notice-error';
    $message = 'O Plugin "Bold Page Builder" é necessário estar instalado e ativo';

    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
  }

  if ( !is_plugin_active( 'smart-slider-3/smart-slider-3.php' )) {
    $class = 'notice notice-error';
    $message = 'O Plugin "Smart Slider" é necessário estar instalado e ativo';

    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
  }    
  if ( !is_plugin_active( 'advanced-custom-fields-pro/acf.php' )) {
    $class = 'notice notice-error';
    $message = 'O Plugin "Advanced Custom Fields Pro" é necessário estar instalado e ativo';

    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
  }


}
add_action( 'admin_notices', 'sample_admin_notice__error' );

add_action( 'wp_ajax_siteWideMessage', 'wpse_sendmail' );
add_action( 'wp_ajax_nopriv_siteWideMessage', 'wpse_sendmail' );


function custom_login_logo() {
  echo '<style type="text/css">
  h1 a { 
    background-image: url(' .get_template_directory_uri().'/img/logo.png?v1) !important;
    background-size: contain !important;
    width: 100% !important;
    background-position: center !important;
  }
  </style>';
}
add_action('login_head', 'custom_login_logo');



function wpse_sendmail()
{
    $to = 'contato@transeuropa.com.br'; // contato@transeuropa.com.br
    $subject = 'Contato via site';
    $body = $_POST['message'];
    $headers = array('Content-Type: text/html; charset=UTF-8');
    
    $resultEmail = wp_mail( $to, $subject, $body, $headers );
    if($resultEmail)
    {
      echo "Enviado com sucesso";
    }
    else {
      echo "Falhou algo";
    }

    die();
  }


  function autenticar()
  {
   if( !is_user_logged_in() ) {
    wp_redirect( home_url("area-do-agente") );
    exit;
  }
}

if ( ! function_exists( 'ld_modify_contact_methods' ) ) :

  function ld_modify_contact_methods( $contactmethods ) {
    $contactmethods['nome_da_agencia'] = __( 'Nome da Agência' );
    $contactmethods['cpf_cnpj'] = __( 'CPF / CNPJ ' );
    $contactmethods['endereco'] = __( 'Endereço' );
    $contactmethods['cidade'] = __( 'Cidade' );
    $contactmethods['CEP'] = __( 'CEP' );
    $contactmethods['estado'] = __( 'Estado' );
    $contactmethods['tel_ddd'] = __( 'Telefone com DDD' );
    $contactmethods['tipo_user'] = __( 'Agente de viagem ou Free lancer?' );


    return $contactmethods;
  }
  add_filter('user_contactmethods','ld_modify_contact_methods', 10, 1);

endif;

if ( ! function_exists( 'retrieve_password2' ) ) :

  function retrieve_password2() {
    $errors = new WP_Error();

    if ( empty( $_POST['user_login'] ) || ! is_string( $_POST['user_login'] ) ) {
      $errors->add('empty_username', __('<strong>ERROR</strong>: Enter a username or email address.'));
    } elseif ( strpos( $_POST['user_login'], '@' ) ) {
      $user_data = get_user_by( 'email', trim( wp_unslash( $_POST['user_login'] ) ) );
      if ( empty( $user_data ) )
        $errors->add('invalid_email', __('<strong>ERROR</strong>: There is no user registered with that email address.'));
    } else {
      $login = trim($_POST['user_login']);
      $user_data = get_user_by('login', $login);
    }

  /**
   * Fires before errors are returned from a password reset request.
   *
   * @since 2.1.0
   * @since 4.4.0 Added the `$errors` parameter.
   *
   * @param WP_Error $errors A WP_Error object containing any errors generated
   *                         by using invalid credentials.
   */
  do_action( 'lostpassword_post', $errors );

  if ( $errors->get_error_code() )
    return $errors;

  if ( !$user_data ) {
    $errors->add('invalidcombo', __('<strong>ERROR</strong>: Invalid username or email.'));
    return $errors;
  }

  // Redefining user_login ensures we return the right case in the email.
  $user_login = $user_data->user_login;
  $user_email = $user_data->user_email;
  $key = get_password_reset_key( $user_data );

  if ( is_wp_error( $key ) ) {
    return $key;
  }

  if ( is_multisite() ) {
    $site_name = get_network()->site_name;
  } else {
    /*
     * The blogname option is escaped with esc_html on the way into the database
     * in sanitize_option we want to reverse this for the plain text arena of emails.
     */
    $site_name = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
  }

  $message = __( 'Someone has requested a password reset for the following account:' ) . "\r\n\r\n";
  /* translators: %s: site name */
  $message .= sprintf( __( 'Site Name: %s'), $site_name ) . "\r\n\r\n";
  /* translators: %s: user login */
  $message .= sprintf( __( 'Username: %s'), $user_login ) . "\r\n\r\n";
  $message .= __( 'If this was a mistake, just ignore this email and nothing will happen.' ) . "\r\n\r\n";
  $message .= __( 'To reset your password, visit the following address:' ) . "\r\n\r\n";
  $message .= '<' . network_site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' ) . ">\r\n";

  /* translators: Password reset email subject. %s: Site name */
  $title = sprintf( __( '[%s] Password Reset' ), $site_name );

  /**
   * Filters the subject of the password reset email.
   *
   * @since 2.8.0
   * @since 4.4.0 Added the `$user_login` and `$user_data` parameters.
   *
   * @param string  $title      Default email title.
   * @param string  $user_login The username for the user.
   * @param WP_User $user_data  WP_User object.
   */
  $title = apply_filters( 'retrieve_password_title', $title, $user_login, $user_data );

  /**
   * Filters the message body of the password reset mail.
   *
   * If the filtered message is empty, the password reset email will not be sent.
   *
   * @since 2.8.0
   * @since 4.1.0 Added `$user_login` and `$user_data` parameters.
   *
   * @param string  $message    Default mail message.
   * @param string  $key        The activation key.
   * @param string  $user_login The username for the user.
   * @param WP_User $user_data  WP_User object.
   */
  $message = apply_filters( 'retrieve_password_message', $message, $key, $user_login, $user_data );

  if ( $message && !wp_mail( $user_email, wp_specialchars_decode( $title ), $message ) )
    wp_die( __('The email could not be sent.') . "<br />\n" . __('Possible reason: your host may have disabled the mail() function.') );

  return true;
}


endif;



function get_version()
{   
    //return rand();
  return wp_get_theme()->get( 'Version' );
}


?>
